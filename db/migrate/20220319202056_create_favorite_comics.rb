class CreateFavoriteComics < ActiveRecord::Migration[7.0]
  def change
    create_table :favorite_comics do |t|
      t.integer :marvel_id

      t.timestamps
    end
  end
end
