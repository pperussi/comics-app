# frozen_string_literal: true

module MarvelComicsApi
  class GetComicsList < MarvelComicsApi::Base
    private
      def request
        Rails.cache.fetch("#{timestamp}/comics_list", expires_in: 30.minutes) do
          Faraday.get(url, query_params)
        end
      end

      def path
        "/v1/public/comics"
      end

      def query_params
        default_hash = {
          limit: ProcessApiResponse::FilterComicsInformation::LIMIT_PER_PAGE,
          orderBy: "-focDate",
          noVariants: true
        }

        hash_with_params = default_hash.merge(params) if params.present?

        hash_with_params.present? ? hash_with_params.merge(auth_params) : default_hash.merge(auth_params)
      end

      def url
        "https://gateway.marvel.com/v1/public/comics"
      end
  end
end
