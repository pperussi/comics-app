# frozen_string_literal: true

module MarvelComicsApi
  class SearchIdByCharactersName < MarvelComicsApi::Base
    private
      def request
        Rails.cache.fetch("#{timestamp}/character_ids", expires_in: 30.minutes) do
          Faraday.get(url, query_params)
        end
      end

      def path
        "/v1/public/characters"
      end

      def query_params
        {
          nameStartsWith: params[:name]
        }.merge(auth_params)
      end
  end
end
