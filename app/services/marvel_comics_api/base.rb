# frozen_string_literal: true

require "faraday"
require "json"

module MarvelComicsApi
  class Base
    def initialize(**params)
      @params = params[:params]
    end

    def call
      response = request

      JSON.parse(response.body).deep_symbolize_keys
    end

    private
      attr_reader :params

      def url
        "#{ENV["MARVEL_COMICS_API_HOST"]}#{path}"
      end

      def auth_params
        {
          ts: timestamp,
          apikey: public_key,
          hash: md5_hash
        }
      end

      def timestamp
        Time.zone.now.to_i
      end

      def public_key
        ENV["MARVEL_PUBLIC_KEY"]
      end

      def md5_hash
        Digest::MD5.hexdigest("#{timestamp}#{private_key}#{public_key}")
      end

      def private_key
        ENV["MARVEL_PRIVATE_KEY"]
      end
  end
end
