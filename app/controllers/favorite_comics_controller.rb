# frozen_string_literal: true

class FavoriteComicsController < ApplicationController
  def create
    @favorite_comic = FavoriteComic.create(marvel_id: params[:marvel_id])

    redirect_back fallback_location: root_path
  end

  def destroy
    @favorite_comic = FavoriteComic.find_by(marvel_id: params[:marvel_id])
    @favorite_comic.destroy

    redirect_back fallback_location: root_path
  end

  private
    def favorite_comic_params
      params.require(:favorite_comic).permit(:marvel_id)
    end
end
