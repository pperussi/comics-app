# frozen_string_literal: true

class ComicsController < ApplicationController
  before_action :set_page_number, only: [:index, :show]
  before_action :get_characters_ids, only: [:show]

  def index
    @comics = ProcessApiResponse::FilterComicsInformation.new(@page).call

    if @comics.nil?
      redirect_back fallback_location: root_path
    else
      favorites_list
    end
  end

  def show
    if @characters_ids.empty?
      flash[:notice] = "Character not found"
      redirect_back fallback_location: root_path
    else
      @comics = ProcessApiResponse::FilterComicsInformation.new(@page, @characters_ids).call

      favorites_list
    end
  end

  private
    def set_page_number
      return @page = params[:page] if params[:page].present? && params[:page].to_i >= 0

      @page = 0
    end

    def get_characters_ids
      @characters_ids ||= ProcessApiResponse::FilterCharactersId.new(params[:character_name]).call
    end

    def favorites_list
      @favorites_list = ProcessApiResponse::FilterFavoriteComics.new(@comics).call
    end
end
