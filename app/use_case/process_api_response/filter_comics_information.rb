# frozen_string_literal: true

module ProcessApiResponse
  class FilterComicsInformation
    LIMIT_PER_PAGE = 30

    def initialize(page, character_ids = nil)
      @page = page
      @character_ids = character_ids
    end

    def call
      results = comics_data(params)

      return if results.empty? || results.nil?

      results.map do |result|
        {
          id: result[:id],
          title: result[:title].upcase,
          image: "#{result[:thumbnail][:path]}.#{result[:thumbnail][:extension]}",
        }
      end
    end

    private
      attr_reader :page, :character_ids

      def comics_data(params)
        MarvelComicsApi::GetComicsList.new(params:).call[:data][:results]
      end

      def params
        hash_params = page != 0 ? offset : {}

        return hash_params.merge(characters) if character_ids.present?

        hash_params
      end

      def offset
        { offset: LIMIT_PER_PAGE * page.to_i }
      end

      def characters
        { characters: character_ids }
      end
  end
end
