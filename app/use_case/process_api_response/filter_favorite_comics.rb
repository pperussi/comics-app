# frozen_string_literal: true

module ProcessApiResponse
  class FilterFavoriteComics
    def initialize(results)
      @results = results
    end

    def call
      comics_ids = results.map { |result| result[:id] }

      search_for_ids(comics_ids)
    end

    private
      attr_reader :results

      def search_for_ids(comics_ids)
        favorites = FavoriteComic.where(marvel_id: comics_ids)

        favorites.map { |favorite| favorite.marvel_id } || []
      end
  end
end
