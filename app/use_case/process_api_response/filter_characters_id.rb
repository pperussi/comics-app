# frozen_string_literal: true

module ProcessApiResponse
  class FilterCharactersId
    def initialize(character_name)
      @character_name = character_name
    end

    def call
      results = search_id_results(params)

      results.map { |result| result[:id] }
    end

    private
      attr_reader :page, :character_name

      def params
        capitalized_name = character_name.split(" ").map { |word| word.capitalize }.join(" ")

        { name: capitalized_name }
      end

      def search_id_results(params)
        MarvelComicsApi::SearchIdByCharactersName.new(params:).call[:data][:results]
      end
  end
end
