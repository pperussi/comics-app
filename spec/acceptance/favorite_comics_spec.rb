# frozen_string_literal: true

require "rails_helper"

RSpec.describe "favorite_comics/create", type: :request do
  describe "POST #create" do
    subject(:new_favorite) do
      post "/favorite_comics/create"
    end

    let(:params) { { marvel_id: 123456 } }

    it { expect { new_favorite }.to change { FavoriteComic.count }.by(1) }
  end

  describe "DELETE #destroy" do
    subject(:delete_favorite) do
      delete "/favorite_comics/destroy", params:
    end

    let(:id) { 123456 }
    let(:params) { { marvel_id: id } }

    before { create(:favorite_comic, marvel_id: id) }

    it { expect { delete_favorite }.to change { FavoriteComic.count }.by(-1) }
  end
end
