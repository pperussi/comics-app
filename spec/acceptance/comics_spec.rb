# frozen_string_literal: true

require "rails_helper"

RSpec.describe "/comics", type: :api do
  let(:filter_favorites_double) { instance_double(ProcessApiResponse::FilterFavoriteComics, call: []) }

  describe "GET #search" do
    subject(:search_by_name) do
      get "/search", params:
    end

    before do
      allow(ProcessApiResponse::FilterFavoriteComics).to receive(:new).and_return(filter_favorites_double)
      allow(ProcessApiResponse::FilterComicsInformation).to receive(:new).and_call_original
    end

    let(:result) do
      [
        { id: 123456, title: "MARVEL PREVIEWS (2017)", image: "http://somepath.com/image.jpg" },
        { id: 456789, title: "THE INCORRIGIBLE HULK (2004) #1", image: "http://someotherpath.com/image.jpg" }
      ]
    end

    let(:params) { { character_name: "joker" } }

    it { expect(ProcessApiResponse::FilterComicsInformation).not_to have_received(:new) }
  end
end
