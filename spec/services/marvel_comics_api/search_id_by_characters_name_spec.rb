# frozen_string_literal: true

require "rails_helper"

describe MarvelComicsApi::SearchIdByCharactersName, :vcr do
  subject { described_class.new(params:) }

  describe "#call" do
    context "when the request is successfull" do
      Timecop.freeze(2022, 1, 1).noon

      let(:params) { { name: "Spider-Man" } }
      let(:response) { subject.call }

      it { expect(response[:data][:results].first[:name]).to include("Spider-Man") }
    end

    context "when dos not found comics with characters name" do
      Timecop.freeze(2022, 1, 1).noon

      let(:params) { { name: "Batman" } }
      let(:response) { subject.call }

      it { expect(response[:data][:results]).to eq([]) }
    end
  end
end
