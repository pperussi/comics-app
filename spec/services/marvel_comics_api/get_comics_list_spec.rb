# frozen_string_literal: true

require "rails_helper"

describe MarvelComicsApi::GetComicsList, :vcr do
  subject { described_class.new(params:) }

  describe "#call" do
    context "when the base request is made" do
      Timecop.freeze(2022, 1, 1).noon

      let(:params) { {} }
      let(:response) { subject.call }

      it "returns comics successfully" do
        expect(response[:code]).to be 200
        expect(response[:data][:results]).to_not eq([])
      end
    end

    context "when request with offset param" do
      Timecop.freeze(2022, 1, 1).noon

      let(:params) do
        { offset: 30 }
      end

      let(:base_results) {  described_class.new.call[:data][:results] }
      let(:base_results_ids) { base_results.map { |result| result[:id] } }
      let(:offset_results) {  subject.call[:data][:results] }
      let(:offset_results_ids) { offset_results.map { |result| result[:id] } }

      it { expect(offset_results_ids).to_not match_array(base_results_ids) }
    end

    context "when request with character ids param" do
      Timecop.freeze(2022, 1, 1).noon

      let(:params) { { characters: [1009515, 1009517, 1017306] } }
      let(:response) {  subject.call }

      it { expect(response[:code]).to be 200 }
    end
  end
end
