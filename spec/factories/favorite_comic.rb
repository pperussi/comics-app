# frozen_string_literal: true

FactoryBot.define do
  factory :favorite_comic do
    marvel_id { rand(100 .. 5000) }
  end
end
