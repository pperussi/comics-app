# frozen_string_literal: true

require "rails_helper"

describe ProcessApiResponse::FilterCharactersId do
  subject { described_class.new(name) }

  describe "#call" do
    let(:response) do
      { data: { results: [ { id: 123456 }, { id: 789123 }, { id: 456789 } ] } }
    end

    let(:get_comics_double) { instance_double(MarvelComicsApi::SearchIdByCharactersName, call: response) }

    context "when process the api response" do
      let(:name) { "deadpool" }

      let(:result) { [123456, 789123, 456789] }

      before do
        allow(MarvelComicsApi::SearchIdByCharactersName).to receive(:new).and_return(get_comics_double)
      end

      it { expect(subject.call).to eq(result) }
    end
  end
end
