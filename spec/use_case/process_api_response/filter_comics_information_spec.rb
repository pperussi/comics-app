# frozen_string_literal: true

require "rails_helper"

describe ProcessApiResponse::FilterComicsInformation do
  subject { described_class.new(page) }

  describe "#call" do
    let(:get_comics_double) { instance_double(MarvelComicsApi::GetComicsList, call: response) }

    context "when process the api response" do
      let(:response) do
        { data: { results: [
          {
            id: 123456,
            title: "Marvel Previews (2017)",
            thumbnail: { path: "http://somepath.com/image", extension: "jpg" }
          },
          {
            id: 456789,
            title: "The Incorrigible Hulk (2004) #1",
            thumbnail: { path: "http://someotherpath.com/image", extension: "jpg" }
          },
        ] } }
      end

      let(:get_comics_double) { instance_double(MarvelComicsApi::GetComicsList, call: response) }

      let(:page) { 1 }

      let(:result) do
        [
          { id: 123456, title: "MARVEL PREVIEWS (2017)", image: "http://somepath.com/image.jpg" },
          { id: 456789, title: "THE INCORRIGIBLE HULK (2004) #1", image: "http://someotherpath.com/image.jpg" }
        ]
      end

      before do
        allow(MarvelComicsApi::GetComicsList).to receive(:new).and_return(get_comics_double)
      end

      it { expect(subject.call).to eq(result) }
    end

    context "when search for character that does not exist" do
      let(:response) { { data: { results: [] } } }

      let(:page) { 1 }

      before do
        allow(MarvelComicsApi::GetComicsList).to receive(:new).and_return(get_comics_double)
      end

      it { expect(subject.call).to eq(nil) }
    end
  end
end
