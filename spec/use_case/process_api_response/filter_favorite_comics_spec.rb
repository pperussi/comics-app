# frozen_string_literal: true

require "rails_helper"

describe ProcessApiResponse::FilterFavoriteComics do
  subject { described_class.new(results) }

  describe "#call" do
    let(:results) do
      [ { id: 123456 }, { id: 789123 }, { id: 789456 }, { id: 456789 } ]
    end

    context "when search for favorite comics in database" do
      let(:id) { 789456 }

      before { create(:favorite_comic, marvel_id: id) }

      it { expect(subject.call).to eq([id]) }
    end

    context "when no id matchs" do
      let(:id) { 19374628 }

      before { create(:favorite_comic, marvel_id: id) }

      it { expect(subject.call).to eq([]) }
    end
  end
end
