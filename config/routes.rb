Rails.application.routes.draw do
  root "comics#index"
  get "/search", to: "comics#show"
  post "favorite_comics/create", to: "favorite_comics#create"
  delete "favorite_comics/destroy", to: "favorite_comics#destroy"
end
