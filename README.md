# Comics App

## Introduction

This project is the first version of a online Marvel's comics list, to consult and favorite the ones the users likes most. The project is a code chalenge from StreetBees  Ruby/Rails Engineer.

Api doc: https://developer.marvel.com/docs

## Getting started

Seting up local development:

- To be able to make the request the sample has to be removed of `.env.sample` file and is necessary to add the private key to access Marvel's API in the variable, not present for security reasons  (https://developer.marvel.com/documentation/authorization)

- To run rails local server:
```bash
docker-compose build .
docker-compose up
```

OBS: Rails caching is on for default on development environment

## Monitoring performance

During the development, I have noticed that the performance of the application is too slow and that worried me. Then I decided to add the gems `rack-mini-profile` and `stackprof` to try to discover the source of the "laziness".

- Observing the `mini-rack` reports I decided to try to change the gem for `HTTP` requests because it seems to be the lowest part of the flux

![](app/assets/images/monitoring3.png)

- `HTTParty` gem was removed and the `Faraday` was added
- Initially, it seems to improve the performance, allowing it with caching the external requests with Redis and cookies

![](app/assets/images/monitoring8.png)

- But it continues to be very slow at some points, unfortunately

![](app/assets/images/monitoring7.png)

- Decided to see if the problem was somewhere in the flux after the request was to use a "mocked" response instead of a request

![](app/assets/images/monitoring9.png)

- The `curl` request results show some laziness too

![](app/assets/images/photo_2022-03-21_09-08-19.jpg)

- So maybe the cache is not working as it is supposed to be, but even if it was the first request is still very low, which lead us to the next steps

## Next Steps

- Write some test for Rails caching to guarantee it is working
- Try to improve de performance of the requests
- Add `Nginx` to the project to work as a load balancer to Rails application in production
- Use dynamic search and favorites flux to avoid reloading the pages
- Check the offset search. It is returning repeated comics
- Maybe feature tests?